;;; dupfilter.cl
;;; Shane Hale (smh1931@cs.rit.edu)
;;;
;;; Description:
;;;  Returns a version of that list with top-level duplicate elements removed

;;;
;;; dupfilter
;;; Arguments:
;;;  lst - the list to remove duplicates
;;; Returns:
;;;  list with the duplicate items removed
;;;
(defun dupfilter (lst)
    (cond
        ;base cases
        ((not (listp lst)) lst)
        ((null lst) NIL)

        ;call the helper function
        (T (helper lst '()))
    )
)

;;;
;;; helper
;;; Description:
;;;  Loops once for each object in the list. If the object doesn't
;;;  exist in the list, this will add it, and return the new list
;;; Arguments:
;;;  lst - the list to remove duplicates
;;;  newLst - the new list, with the dusplaicates removed
;;; Returns:
;;;  List with the duplicate items removed
;;;
(defun helper (lst newLst)
    (cond
        ;base cases
        ((not (listp lst)) NIL)
        ((null lst) newLst)

        ;call the helper function for each sub item in the list
        ;append the returned value to the newLst
        ((not (alreadyContains newLst (car lst)))
         (helper (cdr lst) (append newLst (cons(car lst) '())))
        )

        ;call the helper function as a last resort
        (T (helper (cdr lst) newLst))
    )
)

;;;
;;; alreadyContains
;;; Description:
;;;  Loop through the list, to see if there are duplicate occurances of an item
;;; Arguments:
;;;  lst - the list to temove duplicates
;;;  item - the item to check (to see if there are any duplicate copies of it)
;;; Returns:
;;;  T - if there are more than one copies of item in the list (lst)
;;;  NIL - if there are no other copies of item in the list (lst)
;;;
(defun alreadyContains (lst item)
    (cond
        ;base cases
        ((null lst) NIL)
        ((equal (car lst) item) T)

        ;check any sub lists in the list
        (T (alreadyContains (cdr lst) item))
    )
)
